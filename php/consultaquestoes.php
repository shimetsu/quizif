<?php
ini_set("display_errors", 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$hostname = "localhost"; 
$user = "root";
$password = "ifsp";
$database = "quizif";

$conn = mysqli_connect($hostname, $user, $password, $database);

if (!$conn) {
    die("Conexão falhou: " . mysqli_connect_error());
}

$query = "select * from questoes;";
$results = mysqli_query($conn, $query);
$index = 0;
while ($record = mysqli_fetch_row($results)) {
    $question = array(
        'id' => $record[0],
        'titulo' => $record[1],
        'descricao' => $record[2],
        'disciplina' => $record[3],
        'dificuldade' => $record[4]
    );
    $questions[$index] = $question;
    $index++;
}

mysqli_close($conn);
$formattedData =  json_encode($questions, JSON_PRETTY_PRINT);

echo $formattedData;
?>
