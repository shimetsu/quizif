CREATE DATABASE IF NOT EXISTS quizif;

USE quizif;

CREATE TABLE IF NOT EXISTS questoes(
id int(11) primary key auto_increment,
titulo varchar(100) not null,
descricao varchar(1024) not null,
disciplina varchar(32) not null,
dificuldade varchar(13) not null,

opcaoa varchar(50) not null,
opcaob varchar(50) not null,
opcaoc varchar(50) not null,
opcaod varchar(50) not null,
opcaoe varchar(50) not null,

opcaocorreta varchar(1) not null,
data_armazenamento timestamp DEFAULT CURRENT_TIMESTAMP
);

select * from questoes;
describe questoes;